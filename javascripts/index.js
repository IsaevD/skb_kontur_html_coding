function Matrix(id) {

    this.id = id;
    this.object = $(id);
    var name = this.object.attr("data-matrix-name");

    var rowClassName = "matrix__row";
    var colClassName = "matrix__col";

    this.rowClass = "." + rowClassName;
    this.colClass = "." + colClassName;

    var htmlRowElement = "<div class='" + rowClassName + "'></div>";
    var htmlColumnElement = "<input type='text' class='input_type_text " + colClassName + "'>";
    var htmlDisableColumnElement = "<input disabled type='text' class='input_type_text input_disable " + colClassName + "'>";

    this.getRows = function() {
        return this.object.find(this.rowClass);
    };

    this.getMatrixAsArray = function() {
        var array = new Array();
        var rows = this.getRows();
        for(var i = 0; i < this.getRowCount(); i++){
            array[i] = new Array();
            var row = rows[i];
            for(var j = 0; j < this.getColumnCount(); j++){
                var colValue = $($(row).find(this.colClass)[j]).val();
                if (colValue == "")
                    array[i][j] = 0;
                else
                    array[i][j] = parseFloat(colValue);
            }
        }
        return array;
    };


    this.fillMatrixFromArray = function(array) {
        var rows = this.getRows();
        for (var i = 0; i < this.getRowCount(); i++) {
            for (var j = 0; j < this.getColumnCount(); j++) {
                $($(rows[i]).find(this.colClass)[j]).val(array[i][j]);
            }
        }
    };

    this.getRowCount = function() {
        return this.object.find(this.rowClass).size();
    };

    this.getColumnCount = function() {
        return this.object.find(this.colClass).size() / this.getRowCount();
    };


    this.addNewColumn = function(disabled, callback) {
        disabled = typeof disabled !== 'undefined' ? disabled : false;
        var rows = this.getRows();
        if (rows.first().find(this.colClass).size() < 10) {
            var columnCount = this.getColumnCount() + 1;
            rows.each(function () {
                if (disabled)
                    htmlStr = htmlDisableColumnElement;
                else
                    htmlStr = htmlColumnElement;
                $(htmlStr).appendTo(this).attr("placeholder", name + "," + ($(this).index() + 1) + "," + columnCount);
                //$(this).append(htmlColumnElement);
            });
            if (callback && typeof(callback) === "function") {
                callback();
            }
        } else {
            setMessageError("Количество столбцов матрицы не может быть > 10.");
        }
    };

    this.removeColumn = function(callback) {
        var rows = this.getRows();
        if (rows.first().find(this.colClass).size() > 2) {
            rows.each(function () {
                $(this).find("." + colClassName).last().remove();
            });
            if (callback && typeof(callback) === "function") {
                callback();
            }
        } else {
            setMessageError("Количество столбцов матрицы не может быть < 2.");
        }
    };

    this.addNewRow = function(disabled, callback) {
        disabled = typeof disabled !== 'undefined' ? disabled : false;
        var rows = this.getRows();
        if (rows.size() < 10) {
            this.object.append(htmlRowElement);
            var appendedObject = $(this.getRows().last());
            var rowIndex = appendedObject.index();
            for (i = 1; i < this.getColumnCount() + 1; i++) {
                if (disabled)
                    htmlStr = htmlDisableColumnElement;
                else
                    htmlStr = htmlColumnElement;
                $(htmlStr).appendTo(appendedObject).attr("placeholder", name + "," + rowIndex + "," + i);
            }
            if (callback && typeof(callback) === "function") {
                callback();
            }
        } else {
            setMessageError("Количество строк матрицы не может быть > 10.");
        }
    };

    this.removeRow = function(callback) {
        var rows = this.getRows();
        if (rows.size() > 2) {
            rows.last().remove();
            if (callback && typeof(callback) === "function") {
                callback();
            }
        } else {
            setMessageError("Количество строк матрицы не может быть < 2.");
        }
    };
}

var matrixA = new Matrix("#matrix-a");
var matrixB = new Matrix("#matrix-b");
var matrixResult = new Matrix("#matrix-result");

$(function(){
    setRangeNumericFormatToInput();
    setActivePanel();
    workWithRowsAndColumnsMatrices();
    $("#exchange-matrices").on("click", function(){
        clearMessageError();
        exchangeMatrices(matrixA, matrixB, matrixResult);
    });
    $("#clear-matrices").on("click", function(){
        clearMessageError();
        clearMatrices([matrixA, matrixB, matrixResult]);
    });
    $("#multiplication-matrices").on("click", function(){
        clearMessageError();
        multiplicationMatrices(matrixA, matrixB, matrixResult);
    });
});

function workWithRowsAndColumnsMatrices() {
    $("#add-row").on("click", function() {
        clearMessageError();
        if ($("#is-matrix-a").is(":checked"))
            matrixA.addNewRow(false, function(){ matrixResult.addNewRow(true); });
        else
        if ($("#is-matrix-b").is(":checked"))
            matrixB.addNewRow();
    });
    $("#remove-row").on("click", function() {
        clearMessageError();
        if ($("#is-matrix-a").is(":checked"))
            matrixA.removeRow(function(){ matrixResult.removeRow();});
        else
        if ($("#is-matrix-b").is(":checked"))
            matrixB.removeRow();
    });
    $("#add-col").on("click", function() {
        clearMessageError();
        if ($("#is-matrix-a").is(":checked"))
            matrixA.addNewColumn();
        else
        if ($("#is-matrix-b").is(":checked"))
            matrixB.addNewColumn(false, function(){ matrixResult.addNewColumn(true); });
    });
    $("#remove-col").on("click", function() {
        clearMessageError();
        if ($("#is-matrix-a").is(":checked"))
            matrixA.removeColumn();
        else
        if ($("#is-matrix-b").is(":checked"))
            matrixB.removeColumn(function(){ matrixResult.removeColumn(); });
    });
}

function exchangeMatrices(matrix1, matrix2, matrixResult) {
    var matrix1Rows = matrix1.getRows();
    var matrix2Rows = matrix2.getRows();
    matrix1.object
        .append(matrix2Rows);
    matrix2.object
        .append(matrix1Rows);

    matrixResult.getRows().remove();
    for(var i = 0; i < matrix1.getRowCount(); i++) {
        matrixResult.addNewRow();
    }
    for(var j = 0; j < matrix2.getColumnCount(); j++) {
        matrixResult.addNewColumn(true);
    }
}

function clearMatrices(matrixArray) {
    matrixArray.forEach(function(item, i, arr) {
        item.object.find(item.colClass).val("");
    });
}

function multiplicationMatrices(matrix1, matrix2, matrixResult) {
    var matrix1ColCount = matrix1.getColumnCount();
    var matrix2RowCount = matrix2.getRowCount();
    if (matrix1ColCount != matrix2RowCount) {
        setMessageError("Такие матрицы нельзя перемножить, так как количество столбцов матрицы А не равно количеству строк матрицы B.");
        return false
    } else {
        var matrix1Arr = matrix1.getMatrixAsArray();
        var matrix2Arr = matrix2.getMatrixAsArray();
        var matrixResultArr = new Array();
        var matrix1RowCount = matrix1.getRowCount();
        var matrix2ColCount = matrix2.getColumnCount();
        for (var i = 0; i < matrix1RowCount; i++) {
            matrixResultArr[i] = new Array();
            for (var j = 0; j < matrix2ColCount; j++) {
                matrixResultArr[i][j] = 0;
            }
        }
        for (var i = 0; i < matrix1RowCount; i++) {
            for (var j = 0; j < matrix2ColCount; j++) {
                var total = 0;
                for (var k = 0; k < matrix1ColCount; k++) {
                    total = total + matrix1Arr[i][k]*matrix2Arr[k][j];
                }
                matrixResultArr[i][j] = total;
            }
        }
        matrixResult.fillMatrixFromArray(matrixResultArr);
        return matrixResultArr;
    }
}

function setMessageError(errorText) {
    $(".side-pnl").addClass("side-pnl_error");
    $("#matrices-error").addClass("message_display");
    $("#matrices-error").text(errorText);
}

function clearMessageError() {
    $(".side-pnl").removeClass("side-pnl_error");
    $("#matrices-error").removeClass("message_display");
    $("#matrices-error").text("");
}

function setActivePanel() {
    $(".matrix")
        .on("focusin", ".input_type_text", function(){
            clearMessageError();
            $(".side-pnl").addClass("side-pnl_active");
        })
        .on("focusout", ".input_type_text", function(){
            $(".side-pnl").removeClass("side-pnl_active");
        });
}

function setRangeNumericFormatToInput() {
    $("body").on("change", ".matrix__col", function(){
        var val = Math.abs(parseInt(this.value, 10) || 1);
        this.value = val > 10 ? 10 : val;
    });
}